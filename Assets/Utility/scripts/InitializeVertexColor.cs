﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeVertexColor : MonoBehaviour
{
    public void SetObjectVertexColor()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;

        Color[] colors = new Color[mesh.vertices.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i].r = 0.05f;
            colors[i].g = 0.0f;

        }
        mesh.colors = colors;
    }
}
