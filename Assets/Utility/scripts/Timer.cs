﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Easing;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System;

public delegate void TimerDelegate();
public delegate void TimerUpdateDelegate(float value);

public class Timer
{
    float _currentTime, _from, _to, _duration, _distance, _delay, _currentDelay;
    bool _isComplete = false, _starts = true, _loops = false;
    Easings.Functions _easingFunction;

    event TimerDelegate onStartDelegateEvent;
    event TimerUpdateDelegate onUpdateDelegateEvent;
    event TimerDelegate onCompleteDelegateEvent;

    public bool isComplete
    {
        get
        {
            return _isComplete;
        }
    }

    public Timer reverse
    {
        get
        {
            Timer reversed = new Timer(_to, _from, _duration);
            reversed.onStartDelegateEvent = onCompleteDelegateEvent;
            reversed.onUpdateDelegateEvent = onUpdateDelegateEvent;
            reversed.onCompleteDelegateEvent = onStartDelegateEvent;

            return reversed;
        }
    }

    public TimerDelegate addOnStartDelegate
    {
        set
        {
            onStartDelegateEvent += value;
        }
    }
    public TimerUpdateDelegate addOnUpdateDelegate
    {
        set
        {
            onUpdateDelegateEvent += value;
        }
    }
    public TimerDelegate addOnCompleteDelegate
    {
        set
        {
            onCompleteDelegateEvent += value;
        }
    }

    public Timer(float duration, Easings.Functions easingFunction = Easings.Functions.Linear, float delay = 0.0f, bool loops = false)
    {
        _InitAll(0.0f, duration, duration, easingFunction, delay, loops);
    }

    public Timer(float from, float to, Easings.Functions easingFunction = Easings.Functions.Linear, float delay = 0.0f, bool loops = false)
    {
        float distance = _to - _from;
        _InitAll(from, to, distance, easingFunction, delay, loops);
    }

    public Timer(float from, float to, float duration, Easings.Functions easingFunction = Easings.Functions.Linear, float delay = 0.0f, bool loops = false)
    {
        _InitAll(from, to, duration, easingFunction, delay, loops);
    }

    public static Timer NormalizedTimer( float duration, Easings.Functions easingFunction = Easings.Functions.Linear, float delay = 0.0f, bool loops = false )
	{
        return new Timer( 0f, 1f, duration, easingFunction, delay, loops );
	}

    void _InitAll(float from, float to, float duration, Easings.Functions easingFunction, float delay, bool loops = false)
    {
        _from = from;
        _to = to;
        _duration = duration;
        _currentTime = 0.0f;
        _distance = _to - _from;
        _easingFunction = easingFunction;
        _delay = delay;
        _currentDelay = 0.0f;
        _loops = loops;
    }

    public void Reset()
    {
        _isComplete = false;
        _currentTime = 0.0f;
    }

    public void Complete()
    {
        _isComplete = true;
        _currentTime = _duration;
    }

    public void Update(float delatTime)
    {
        if (_starts)
        {
            _starts = false;

            if (onStartDelegateEvent != null)
            {
                onStartDelegateEvent.Invoke();
            }
        }

        if (_currentDelay >= _delay)
        {
            _currentTime += delatTime;
            if (_currentTime >= _duration)
            {
                _currentTime = _duration;
                if (_loops)
                {
                    _currentTime = 0.0f;
                }
                else
                {
                    _isComplete = true;
                }
            }

            if (onUpdateDelegateEvent != null)
            {
                float time = (_currentTime / _duration);
                if(float.IsNaN(time))
                {
                    MonoBehaviour.print("is nan " + time);
                    time = 1f;
                }
                onUpdateDelegateEvent.Invoke(_from + Easings.Interpolate(time, _easingFunction) * _distance);
            }
        }
        else
        {
            _currentDelay += delatTime;
        }

        if (_isComplete == true)
        {
            if (onCompleteDelegateEvent != null)
            {
                onCompleteDelegateEvent.Invoke();
            }
        }
    }

    public Task asTask ()
    {
        Task task = new Task(delegate ()
        {
            while (!this.isComplete) ;
        });
        task.Start();
        return task;
    }

    public void play()
    {
        TimerManager.instance.AddTimer(this);
    }

    public void playFixed()
    {
        TimerManager.instance.AddFixedTimer(this);
    }

    public Task playAsync()
    {
        play();
        return asTask();
    }

    public Task playFixedAsync()
    {
        playFixed();
        return asTask();
    }

    public void stop()
    {
        TimerManager.instance.RemoveTimer( this );
    }
}
