﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerManager : MonoBehaviour {

	static TimerManager _instance;

	List<Timer> timerList;
	List<Timer> fixedTimerList;

	public static TimerManager instance
	{
		get
		{
			return _instance;
		}
	}

	private void Awake()
	{
		if( _instance != null)
		{
			throw new System.Exception("TimerManager must be singleton");
		}
		_instance = this;

		timerList = new List<Timer>();
		fixedTimerList = new List<Timer>();
	}

	public void AddTimer( Timer newTimer)
	{
		timerList.Add( newTimer );
	}

	public void AddFixedTimer( Timer newTimer )
	{
		fixedTimerList.Add( newTimer );
	}

	public void RemoveTimer( Timer timer )
	{
		timerList.Remove( timer );
		fixedTimerList.Remove( timer );
	}

	void Update () {

		updateCustomTimerList( timerList );
	}

	void FixedUpdate()
	{
		updateCustomTimerList( fixedTimerList );
    }

	void updateCustomTimerList( List<Timer> customTimerList)
	{
		int timerCount = customTimerList.Count;

		if( timerCount > 0)
		{
			float deltaTime = Time.deltaTime;

			List<Timer> timerToBeRemoved = new List<Timer>();
			// update timers
			for ( int i = 0 ; i < timerCount ; ++i )
			{
				customTimerList[i].Update( deltaTime );

				if ( customTimerList[i].isComplete )
				{
					timerToBeRemoved.Add( customTimerList[i] );
				}
			}

			// remove completed timers
			timerToBeRemoved.ForEach( delegate ( Timer timer )
			{
				customTimerList.Remove( timer );
			} );
		}
	}
}
