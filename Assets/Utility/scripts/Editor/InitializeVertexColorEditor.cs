﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(InitializeVertexColor))]
public class InitializeVertexColorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        InitializeVertexColor script = (target as InitializeVertexColor);

        if (GUILayout.Button("Initialize Vertex Color"))
        {
            script.SetObjectVertexColor();
        }
    }
}
