﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

public class VoxelParser : MonoBehaviour
{
    [SerializeField]
    GameObject[] prefabList;

    [SerializeField]
    public string fileName;

    [SerializeField]
    public GameObject parentTemplate;

    public void GenerateFromParsing()
    {
        // le parsing fonctionne avec un export ply au format Point Clouds
        string[] lines = File.ReadAllLines("Assets/VoxelImporter/VoxelExport/"+fileName+".ply");
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3Int> colors = new List<Vector3Int>();

        int i = 0;

        while (lines[i] != "end_header")
        {
            i++;
        }
        i++;

        //parse les vertices
        while (i < lines.Length && lines[i].Split(' ').Length == 6)
        {
            string[] coord = lines[i].Split(' ');

            //NumberStyles styles = System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float | System.Globalization.NumberStyles.AllowLeadingSign;

            float x = float.Parse(coord[0], CultureInfo.InvariantCulture);
            float y = float.Parse(coord[2], CultureInfo.InvariantCulture);
            float z = float.Parse(coord[1], CultureInfo.InvariantCulture);
            int r = int.Parse(coord[3], CultureInfo.InvariantCulture);
            int g = int.Parse(coord[4], CultureInfo.InvariantCulture);
            int b = int.Parse(coord[5], CultureInfo.InvariantCulture);

            vertices.Add(new Vector3(x, y, z));
            colors.Add(new Vector3Int(r, g, b));

            //i += 24;
            i += 1;
        }

        if(vertices.Count > 0)
		{
            GameObject parent = CreateParent();
            for(i = 0; i< vertices.Count; ++i)
            {
                CreateBrick(vertices[i], colors[i], parent);
            }
        }


    }

    void CreateBrick(Vector3 position, Vector3Int color, GameObject parent)
    {
        //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //cube.GetComponent<BoxCollider>().enabled = false;
        //cube.transform.position = position;

        GameObject template = GetPrefab(color);

        if(template == null)
        {
            print( color + "is not known");
        }

        if(template != null)
        {
            GameObject brick = GameObject.Instantiate(template, parent.transform);
            brick.transform.position = position + Vector3.right * .5f + Vector3.forward;
        }
    }

    GameObject GetPrefab(Vector3Int color)
    {
        Vector3Int brick1 = new Vector3Int(238, 238, 238);
        Vector3Int brick2 = new Vector3Int(238, 0, 0);
        Vector3Int brick3 = new Vector3Int(34, 0, 0);

        Vector3Int[] colorList = { brick1, brick2, brick3 };

        for (int i = 0; i < colorList.Length; ++i)
        {
            if(colorList[i] == color)
            {
                return prefabList[i];
            }
        }
        return null;
    }

    GameObject CreateParent()
	{
        GameObject parent = GameObject.Instantiate( parentTemplate );
        parent.transform.position = Vector3.zero;
        return parent;
	}

}
