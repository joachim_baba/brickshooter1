﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( VoxelParser ) )]
public class VoxelParserEditor : Editor
{
	public override void OnInspectorGUI()
	{
		VoxelParser voxelParser = (target as VoxelParser);

		var serializedObject = new SerializedObject(target);
		var property = serializedObject.FindProperty("prefabList");
		var property2 = serializedObject.FindProperty("fileName");
		serializedObject.Update();
		EditorGUILayout.PropertyField( property, true );
		EditorGUILayout.PropertyField( property2, true );
		serializedObject.ApplyModifiedProperties();

		voxelParser.parentTemplate = EditorGUILayout.ObjectField( "parentTemplate", voxelParser.parentTemplate, typeof( GameObject ), false ) as GameObject;

		if ( GUILayout.Button( "generate model" ) )
		{
			voxelParser.GenerateFromParsing();
		}
	}
}
