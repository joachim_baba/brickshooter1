﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    [SerializeField]
    AudioClip[] audioClips;

    AudioSource audioSource;

    Mesh mesh;

    ParticleSystem _particleSystem;
    Collider _collider;

    bool isDead = false;

    float AnimationDuration = 1.3f;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        mesh = GetComponent<MeshFilter>().mesh;
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _collider = transform.GetComponent<Collider>();
    }

    virtual protected void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ball"))
        {
            ApplyBallHit();
        }
    }

    protected void ApplyBallHit()
    {
        if (this.gameObject != null && !isDead)
        {
            isDead = true;
            if (audioClips.Length > 0)
            {
                AudioClip audioClip = audioClips[Random.Range(0, audioClips.Length)];
                audioSource.PlayOneShot(audioClip);
            }
            _collider.enabled = false;

            Timer disolveTimer = PlayDisolveAnimation();

            disolveTimer.play();
        }
    }

    protected Timer PlayDisolveAnimation()
    {
        if (_particleSystem != null)
            _particleSystem.Emit(15);

        Timer disolveTimer = new Timer(0f, 0.95f, AnimationDuration, Easing.Easings.Functions.ExponentialEaseOut);
        disolveTimer.addOnUpdateDelegate = delegate (float time)
        {
            Color[] colors = mesh.colors;

            for (int i = 0; i < colors.Length; i++)
            {
                colors[i].r = 1 - time;
                colors[i].g = time * 0.1f;
            }

            mesh.colors = colors;

            transform.localScale = Vector3.one * (.5f + time);
        };
        disolveTimer.addOnCompleteDelegate = delegate ()
        {
            GameObject.Destroy(this.gameObject);
        };

        return disolveTimer;
    }
}
