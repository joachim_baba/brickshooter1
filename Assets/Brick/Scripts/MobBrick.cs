﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobBrick : MonoBehaviour
{
    [SerializeField]
    AudioClip[] audioClips;

    AudioSource audioSource;

    Collider _collider;

    bool isDead = false, selectedForDestruction = false;

    float AnimationDuration = 1.3f;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        _collider = transform.GetComponent<Collider>();
    }

    virtual protected void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ball") && !selectedForDestruction)
        {
            ApplyBallHit();
        }
    }

    public void ApplyBallHit()
    {
        if (this != null && this.gameObject != null && !isDead)
        {
            isDead = true;
            if (audioClips.Length > 0 && !selectedForDestruction)
            {
                AudioClip audioClip = audioClips[Random.Range(0, audioClips.Length)];
                audioSource.PlayOneShot(audioClip);
            }
            _collider.enabled = false;

            Timer disolveTimer = PlayDisolveAnimation();

            disolveTimer.play();
        }
    }

    public void SelectForDestruction ()
    {
        selectedForDestruction = true;
    }

    protected Timer PlayDisolveAnimation()
    {

        Timer disolveTimer = new Timer(1.0f, .0f, AnimationDuration, Easing.Easings.Functions.ExponentialEaseOut);
        disolveTimer.addOnUpdateDelegate = delegate (float time)
        {
            if (this != null)
            {
                transform.localScale = Vector3.one * time;
            }
        };
        disolveTimer.addOnCompleteDelegate = delegate ()
        {
            if (this != null)
            {
                GameObject.Destroy(this.gameObject);
            }
        };

        return disolveTimer;
    }
}
