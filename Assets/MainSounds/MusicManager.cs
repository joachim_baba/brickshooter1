﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField]
    AudioClip music, successMusic, failMusic;

    [SerializeField]
    AudioSource audioSourceMusic;

    public float musicVolume = 0.4f;

    private void Start()
    {
        PlayNewMusic();
        audioSourceMusic.loop = true;
    }

    void PlayNewMusic()
    {
        PlayMusic(music);
    }

    public void PlaySuccessMusic()
    {
        PlayMusic(successMusic);
    }

    public void PlayFailMusic()
    {
        PlayMusic(failMusic);
    }

    void PlayMusic(AudioClip newMusic)
    {
        audioSourceMusic.clip = newMusic;
        audioSourceMusic.volume = musicVolume;
        audioSourceMusic.Play();
    }
}
