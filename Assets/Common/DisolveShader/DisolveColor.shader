﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Perso/DisolveColor"
{
	Properties
	{
		_MatColor ("Color", Color) = (1, 1, 1, 1)
		_MatColor2 ("Color2", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
	
		Tags{"Queue" = "Transparent"}
		Pass
		{	
			//directive propre à Unity
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Cull Off
		
			CGPROGRAM
			#pragma vertex vert						//vertex shader function name
			#pragma fragment frag					//fragment shader function name
			#pragma target 5.0						//shader model support

			float4 _MatColor;							//declare a variable that matches the property's name
			float4 _MatColor2;
			
			struct appData							//vertex structure from application
			{
				float4 pos	: POSITION;
				float2 texcoord	: TEXCOORD0;
				fixed4 color : COLOR;
			};

			struct v2f								//structure to pass data from vertex to fragment shader
			{
				float4 pos	: SV_POSITION;
				float2 UV	: ATTR0;
				fixed4 color : COLOR;
			};

			v2f vert(appData v)								//vertex shader function
			{
				v2f o;
				
				o.UV = v.texcoord;
				
				o.pos = UnityObjectToClipPos (v.pos);	//transform and project to view using (Model View Project)
				
				o.color = v.color;
				
				return o;
			}


			float modulo(float a, float b) 
			{
				return a - floor(a / b) * b;
			}

			float4 frag(v2f i) : COLOR						//fragment shader function
			{				
				
				//color.r > 0 < 1
				//color.g > 0 < 0.1
				
				float4 output;
				
				//square sizes
				float modU = modulo( i.UV.x-.5f, i.color.r);
				float modV = modulo( i.UV.y-.5f, i.color.r);
				//step size
				float stepU = step(modU, i.color.g);
				float stepV = step(modV, i.color.g);
				float result = stepU + stepV;
				
				output.rgba = lerp(_MatColor, _MatColor2, clamp( result * (1/i.color.r), 0.0f, 1.0f) );
				
				return output;								//output color
			}
			
			ENDCG
		}
	}
}
