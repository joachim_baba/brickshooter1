﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Perso/Disolve1"
{
	Properties
	{
		_MatColor ("Color", Color) = (1, 1, 1, 1)
		_MatColor2 ("Color2", Color) = (1, 1, 1, 1)
		_RangeMod ("RangeMod", Range(0, 1)) = .25
		_RangeStep ("RangeStep", Range(0, 0.1)) = .125
	}
	SubShader
	{
	
		Tags{"Queue" = "Transparent"}
		Pass
		{	
			//directive propre à Unity
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Cull Off
		
			CGPROGRAM
			#pragma vertex vert						//vertex shader function name
			#pragma fragment frag					//fragment shader function name
			#pragma target 5.0						//shader model support

			float4 _MatColor;							//declare a variable that matches the property's name
			float4 _MatColor2;
			float _RangeMod;
			float _RangeStep;
			
			struct appData							//vertex structure from application
			{
				float4 pos	: POSITION;
				float2 texcoord	: TEXCOORD0;
			};

			struct v2f								//structure to pass data from vertex to fragment shader
			{
				float4 pos	: SV_POSITION;
				float2 UV	: ATTR0;
			};

			v2f vert(appData v)								//vertex shader function
			{
				v2f o;
				
				o.UV = v.texcoord;
				
				o.pos = UnityObjectToClipPos (v.pos);	//transform and project to view using (Model View Project)
				
				return o;
			}


			float modulo(float a, float b) 
			{
				return a - floor(a / b) * b;
			}

			float4 frag(v2f i) : COLOR						//fragment shader function
			{				
				
				float4 color;
				//color.rgb = lerp(_MatColor, _MatColor2, i.pos.y * 0.01f);
				
				float modU = modulo( i.UV.x-.5f, _RangeMod);
				float modV = modulo( i.UV.y-.5f, _RangeMod);
				float stepU = step(modU, _RangeStep);
				float stepV = step(modV, _RangeStep);
				float result = stepU + stepV;
				
				color.rgba = lerp(_MatColor, _MatColor2, clamp( result * (1/_RangeMod), 0.0f, 1.0f) );
				
				//color.r = modulo(i.UV.x, 0.25f);
				//color.gb = float2(.0f, .0f);
				//color.a = 1;
				return color;								//output color
			}
			
			ENDCG
		}
	}
}
