﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorAnim : MonoBehaviour
{
    Mesh mesh;

    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;

        Color[] colors = new Color[mesh.vertices.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i].r = 1f;
            colors[i].g = 0.1f;

        }
        mesh.colors = colors;
    }

    private void Update()
    {
        float deltaTime = Time.deltaTime;

        Color[] colors = mesh.colors;
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i].r -= deltaTime * 0.5f;
            colors[i].g += deltaTime * 0.05f;

            if(colors[i].r < 0.0f)
            {
                colors[i].r = 1f;
            }
            if(colors[i].g > 0.1f)
            {
                colors[i].g = 0f;
            }
        }

        mesh.colors = colors;
    }
}
