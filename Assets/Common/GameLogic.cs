﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    [SerializeField]
    Text CountdownText;

    [SerializeField]
    InGameMenu inGameMenu;

    MusicManager musicManager;

    Timer countdown;

    public static GameLogic instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        musicManager = GetComponent<MusicManager>();

        float maxTime = 60f *3;
        countdown = new Timer(maxTime, delay: 1f);
        countdown.addOnUpdateDelegate = delegate (float t)
        {
            float current = maxTime - t;
            int clampTime = Mathf.FloorToInt(current);
            int minutes = clampTime / 60;
            int seconds = clampTime - (minutes * 60);

            string text = "";
            if(minutes > 0)
            {
                text += minutes + " : ";
            }
            text += seconds;
            CountdownText.text = text;
        };
        countdown.addOnCompleteDelegate = delegate ()
        {
            inGameMenu.GameLost();
            musicManager.PlayFailMusic();
        };
        countdown.play();
    }

    public void AllItemsCollected()
    {
        countdown.stop();
        inGameMenu.GameWon();
        musicManager.PlaySuccessMusic();
    }

    public void PlayerDead()
    {
        countdown.stop();
        inGameMenu.GameLost();
        musicManager.PlayFailMusic();
    }
}
