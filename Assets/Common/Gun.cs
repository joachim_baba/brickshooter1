﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    GameObject BallTemplate;
    [SerializeField]
    float shootForce = 2f;

    AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Shoot(Vector3 position, Vector3 direction)
    {
        GameObject ball = GameObject.Instantiate(BallTemplate, position + direction * 0.1f, Quaternion.identity);
        ball.GetComponent<Rigidbody>().AddForce(direction * shootForce, ForceMode.Impulse);
        audioSource.Play();
    }
}
