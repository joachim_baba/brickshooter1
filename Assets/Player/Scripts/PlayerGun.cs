﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGun : Gun
{
    [SerializeField]
    Transform BallShootPosition;
    [SerializeField]
    Transform CameraPosition;
    [SerializeField]
    float maxRaycastDistance = 50f;
    [SerializeField]
    LayerMask layerMask;

    float shootDelay = 0.2f;

    float nextShootTime = 0;

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(CameraPosition.position, CameraPosition.forward);
        RaycastHit raycastHit;
        Physics.Raycast( ray, out raycastHit, maxRaycastDistance, layerMask );

        Vector3 point = raycastHit.point;

        transform.rotation = Quaternion.LookRotation(point - transform.position, CameraPosition.up);

        if (Input.GetButtonDown("Fire1") && Time.time >= nextShootTime )
        {
            nextShootTime = Time.time + shootDelay;
            Shoot(BallShootPosition.position, transform.forward);
        }
    }
}
