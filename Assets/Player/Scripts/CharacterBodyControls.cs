﻿using System;
using UnityEngine;

public class CharacterBodyControls : MonoBehaviour
{
    CharacterController controller;

    public float speed = 12f;

    //gravity
    public float gravity = -9.81f;
    Vector3 velocity;
    public Transform groundCheck;
    public float groundDistance = .4f;
    public LayerMask groundMask;
    bool isGrounded;

    //jump
    public float jumpHeight = 3f;

    [SerializeField]
    AudioClip[] audioClips;
    [SerializeField]
    float maxStepLength = 2f;

    AudioSource audioSource;
    float stepLength = 0f;

    public static CharacterBodyControls instance { get; private set; }

    private void Awake()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
        controller = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if(isGrounded && velocity.y < .0f)
        {
            velocity.y = -2f;
        }

        float deltaTime = Time.deltaTime;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //displacement
        Vector3 dir = transform.right * horizontal + transform.forward * vertical;
        dir = Vector3.ClampMagnitude(dir, 1f);

        Vector3 translation = dir * deltaTime * speed;
        //controller.Move(dir * deltaTime * speed);

        //step sound
        stepLength += translation.magnitude;
        bool smallStep = dir.magnitude > 0.24f && dir.magnitude < 0.26f;
        if ((stepLength >= maxStepLength || smallStep) && isGrounded)
        {
            stepLength = 0f;
            playStepSound();
        }

        //jump
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        //gravity
        velocity.y += gravity * deltaTime;

        Vector3 upwardTranslation = velocity * deltaTime;
        //controller.Move(velocity * deltaTime);
        controller.Move(translation + upwardTranslation);
    }

    void playStepSound()
    {
        if(!audioSource.isPlaying)
        {
            AudioClip audioClip = audioClips[UnityEngine.Random.Range(0, audioClips.Length)];
            audioSource.PlayOneShot(audioClip);
        }
    }

}

