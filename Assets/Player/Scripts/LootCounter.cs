﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootCounter : MonoBehaviour
{
    [SerializeField]
    Text lootCountText;

    [SerializeField]
    AudioClip[] audioClips;

    [SerializeField]
    AudioSource audioSource;

    int completeLootCount = 0;
    int lootCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        completeLootCount = Loot.lootCount;
        UpdateLootCount();
    }

    void UpdateLootCount()
    {
        completeLootCount = Loot.lootCount;
        if(completeLootCount > 0)
        {
            lootCountText.text = lootCount + "/" + completeLootCount;
        }

        if (lootCount > 0)
        {
            AudioClip audioClip = audioClips[Random.Range(0, audioClips.Length)];
            audioSource.PlayOneShot(audioClip);
        }

        if (completeLootCount > 0 && lootCount >= completeLootCount)
        {
            GameLogic.instance.AllItemsCollected();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Loot"))
        {
            lootCount++;
            UpdateLootCount();
        }
    }
}
