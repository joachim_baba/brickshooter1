﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCameraControls : MonoBehaviour
{
    public Transform playerBody;
    public float mouseSensitivityX = 550f;
    public float mouseSensitivityY = 200f;
    public float mouseMaxClamp = 2f;
    public float dynamicThreshold = 1f;

    float _xAxisRotation = 0f;

    private void Awake()
    {
        mouseSensitivityY = mouseSensitivityX * .65f;
    }

    void Update()
    {
        float deltaTime = Time.deltaTime;

        float mouseX = Mathf.Clamp(Input.GetAxis("Mouse X"), -mouseMaxClamp, mouseMaxClamp);
        float mouseY = Mathf.Clamp(Input.GetAxis("Mouse Y"), -mouseMaxClamp, mouseMaxClamp);
        float threshold = Easing.Easings.ExponentialEaseIn(Mathf.Abs(mouseX) / mouseMaxClamp) * dynamicThreshold;
        mouseX -= threshold;
        //mouseY -= threshold;
        mouseX *= mouseSensitivityX * deltaTime;
        mouseY *= mouseSensitivityY * deltaTime;

        _xAxisRotation -= mouseY;
        _xAxisRotation = Mathf.Clamp(_xAxisRotation, -50f, 60f);

        transform.localRotation = Quaternion.Euler(_xAxisRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
