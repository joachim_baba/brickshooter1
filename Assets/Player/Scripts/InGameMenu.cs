﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour
{
	bool isOn = false;
	bool isLocked = false;
	CharacterBodyControls characterBody;
	[SerializeField]
	CharacterCameraControls characterCamera;
	[SerializeField]
	Button menuButton, menuButton2;

	[SerializeField]
	Text StatusText;

	AudioSource audioSource;

	[SerializeField]
	AudioClip audioSelect, audioCancel;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	// Start is called before the first frame update
	void Start()
	{
		characterBody = GetComponent<CharacterBodyControls>();
	}

	// Update is called once per frame
	void Update()
	{
		if ( Input.GetButtonDown( "Exit" ) && !isLocked)
		{
			ToggleMenu();
		}
	}

	public void ToggleMenu()
	{
		audioSource.PlayOneShot(audioSelect);
		isOn = !isOn;

		characterBody.enabled = !isOn;
		characterCamera.enabled = !isOn;
		menuButton.gameObject.SetActive(isOn);
		menuButton2.gameObject.SetActive(isOn & !isLocked);
		if (isOn)
		{
			Cursor.lockState = CursorLockMode.Confined;
		}
		else
		{
			Cursor.lockState = CursorLockMode.Locked;
		}
	}

	public void GameLost()
    {
		StatusText.color = new Color(1, .2f, .2f);
		StatusText.text = "You Lost\n" +
			"Try Again !";

		isLocked = true;
		if (!isOn)
        {
			ToggleMenu();
        }
	}

	public void GameWon()
	{
		StatusText.color = new Color(.2f, 1, .2f);
		StatusText.text = "You Win\n" +
			"Congratulations !";

		if (!isOn)
		{
			ToggleMenu();
		}
	}

	public void LoadMenu()
	{
		Loot.lootCount = 0;
		audioSource.PlayOneShot(audioCancel);
		string sceneName = "MenuScene";
		SceneManager.LoadScene( sceneName, LoadSceneMode.Single );
	}
}
