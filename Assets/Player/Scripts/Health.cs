﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    float level = 1f;

    [SerializeField]
    Image levelImage;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("BallMob"))
        {
            if(level > 0f)
            {
                level -= 0.1f;
                if(level <= 0f)
                {
                    level = 0f;
                    if(GameLogic.instance != null)
                    {
                        GameLogic.instance.PlayerDead();
                    }
                }
                levelImage.fillAmount = level;
            }
        }
    }
}
