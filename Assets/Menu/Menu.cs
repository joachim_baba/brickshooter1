﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField]
    Canvas mainCanvas, instructionCanvas, creditCanvas;

    AudioSource audioSource;

    [SerializeField]
    AudioClip audioSelect, audioCancel, audioPlay;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void LoadGame()
    {
        audioSource.PlayOneShot(audioPlay);
        string sceneName = "ProcGenMapScene";
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void ShowInstruction(bool _enable = true)
    {
        audioSource.PlayOneShot(_enable ? audioSelect : audioCancel);
        mainCanvas.gameObject.SetActive(!_enable);
        instructionCanvas.gameObject.SetActive(_enable);
    }

    public void ShowCredit(bool _enable = true)
    {
        audioSource.PlayOneShot(_enable ? audioSelect : audioCancel);
        mainCanvas.gameObject.SetActive(!_enable);
        creditCanvas.gameObject.SetActive(_enable);
    }

    public void Exit()
    {
        audioSource.PlayOneShot(audioCancel);
        Application.Quit();
    }
}
