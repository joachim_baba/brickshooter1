﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
	enum TileType
	{
		corner, cross, deadEnd, tSection, wall
	}

	enum TileObstacle
	{
		mobPath, brickWall, none
	}

	class TileData
	{
		public int x, y, rotation;
		public TileType type;
		public TileObstacle obstacle;

		public TileData( TileType newType, int newX, int newY, int newRotation )
		{
			type = newType;
			x = newX;
			y = newY;
			rotation = newRotation;
			obstacle = TileObstacle.none;
		}
	}

	[SerializeField]
	public GameObject mapParent;
	GameObject parent;
	public int width = 8, height = 6;

	const int minimumLineSpace = 3;
	const int maxLineCount = 5;
	int totalLineCount = 0;

	[SerializeField]
	public Material[] testMat;

	[SerializeField]
	public GameObject[] mapPrefabs, wallPrefabs;

	List<int> sections;
	List<List<int>> mobPaths;
	const int playerCornerIndex = 0;

	[SerializeField]
	public MobSpawner mobSpawner;

	int tileSize = 12;

	public void GenerateMap( bool isDebug = false)
	{
		//width & height : min >= 6 && max <= 9
		if(!isDebug)
        {
			width = Random.Range(6, 9);
			height = Random.Range(6, 9);
        }

		TileData[] tilesData = new TileData[width * height];
		sections = new List<int>();
		mobPaths = new List<List<int>>();

		CreateSidesLines( tilesData );
		CreateCorner( tilesData );
		CreateInsideLines( tilesData );

		CreateMobPaths( tilesData );
		CreateWalls( tilesData );

		if( isDebug )
		{
			GenerateDebugTiles( tilesData );
		} else
		{
			GenerateMapTiles( tilesData );
		}
	}

	private void CreateSidesLines( TileData[] tilesData )
	{
		//sides up down
		int[] ys = { 0, height - 1 };

		TileType tileType = TileType.wall;
		for ( int i = 0 ; i < width * ys.Length ; ++i )
		{
			int x = i % width;
			int y = ys[i / width];
			int rotation = y <= 0 ? 0 : 180;
			tilesData[x + y * width] = new TileData( tileType, x, y, rotation );
		}

		//sides left right
		int[] xs = { 0, width - 1 };

		tileType = TileType.wall;
		for ( int i = 0 ; i < height * xs.Length ; ++i )
		{
			int y = i % height;
			int x = xs[i / height];
			int rotation = x <= 0 ? 90 : -90;
			if ( tilesData[x + y * width] == null )
			{
				tilesData[x + y * width] = new TileData( tileType, x, y, rotation );
			}
		}
	}

	private void CreateCorner( TileData[] tilesData )
	{
		//corners
		int rightCorner = width -1;
		int downLeftCorner = width * height - width;
		int downRightCorner = width * height - 1;

		tilesData[0].type = TileType.corner;
		tilesData[0].rotation = 0;
		tilesData[rightCorner].type = TileType.corner;
		tilesData[rightCorner].rotation = 90;
		tilesData[downLeftCorner].type = TileType.corner;
		tilesData[downLeftCorner].rotation = -90;
		tilesData[downRightCorner].type = TileType.corner;
		tilesData[downRightCorner].rotation = 180;

		sections.Add( rightCorner );
		sections.Add( downLeftCorner );
		sections.Add( downRightCorner );
	}

	private void CreateInsideLines( TileData[] tilesData )
	{
		totalLineCount = 0;
		CreateInsideLinesByX( tilesData, width, height );
		CreateInsideLinesByY( tilesData, width, height );
	}

	private void CreateInsideLinesByX( TileData[] tilesData, int _width, int _height )
	{
		int xlineMax = _width - 2;
		int xlineCount = xlineMax;
		int yWallSample = 1;

		while ( xlineCount >= minimumLineSpace && totalLineCount < maxLineCount )
		{
			float rand = Random.value;
			//print(rand);
			if ( xlineCount > minimumLineSpace && rand < 0.5f )
			{
				//MonoBehaviour.print( "pass skipped" );
				xlineCount--;
				continue;
			}
			// loop on line between extremities
			for ( int x = 1 ; x < _width - 1 ; ++x )
			{
				if ( totalLineCount >= maxLineCount )
				{
					break;
				}

				TileData precedentTile = tilesData[yWallSample * _width + (x - 1)];
				TileData nextTile = tilesData[yWallSample * _width + (x + 1)];
				TileData sideATile = tilesData[x];
				TileData sideBTile = tilesData[x + (_height - 1) * _width];

				// choose randomly if this spot is chosen
				rand = Random.value;
				//print(rand);
				if ( xlineCount > minimumLineSpace && rand > 0.5f )
				{
					//MonoBehaviour.print( "line skipped" );
					continue;
				}

				if ( precedentTile == null && nextTile == null && sideATile.type == TileType.wall && sideBTile.type == TileType.wall )
				{
					for ( int y = 0 ; y < _height ; ++y )
					{
						TileData tileData = tilesData[y * _width + x];
						if ( (y == 0 || y == _height - 1) && tileData != null )
						{
							tileData.type = TileType.tSection;
							sections.Add( y * _width + x );
						}
						else if ( tileData != null && tileData.type == TileType.wall )
						{
							tileData.type = TileType.cross;
							sections.Add( y * _width + x );
						}
						else if ( tileData == null )
						{
							tilesData[y * _width + x] = new TileData( TileType.wall, x, y, 90 );
						}

					}
					totalLineCount++;
				}
			}

			xlineCount--;
		}

	}

	private void CreateInsideLinesByY( TileData[] tilesData, int _width, int _height )
	{
		int ylineMax = _height - 2;
		int ylineCount = ylineMax;
		int xWallSample = 1;

		while ( ylineCount >= minimumLineSpace && totalLineCount < maxLineCount )
		{
			float rand = Random.value;
			//print(rand);
			if ( ylineCount > minimumLineSpace && rand < 0.5f )
			{
				ylineCount--;
				continue;
			}
			// loop on line between extremities
			for ( int y = 1 ; y < _height - 1 ; ++y )
			{
				if ( totalLineCount >= maxLineCount )
				{
					break;
				}

				TileData precedentTile = tilesData[xWallSample + (y - 1)*_width];
				TileData nextTile = tilesData[xWallSample + (y + 1)*_width];
				TileData sideATile = tilesData[y*_width];
				TileData sideBTile = tilesData[y*_width + (_width - 1)];

				// choose randomly if this spot is chosen
				rand = Random.value;
				//print(rand);
				if ( ylineCount > minimumLineSpace && rand > 0.5f )
				{
					continue;
				}

				if ( precedentTile == null && nextTile == null && sideATile.type == TileType.wall && sideBTile.type == TileType.wall )
				{
					for ( int x = 0 ; x < _width ; ++x )
					{
						TileData tileData = tilesData[y * _width + x];
						if ( (x == 0 || x == _width - 1) && tileData != null )
						{
							tileData.type = TileType.tSection;
							tileData.rotation += 180;
							sections.Add( y * _width + x );
						}
						else if ( tileData != null && tileData.type == TileType.wall )
						{
							tileData.type = TileType.cross;
							sections.Add( y * _width + x );
						}
						else if ( tileData == null )
						{
							tilesData[y * _width + x] = new TileData( TileType.wall, x, y, 0 );
						}

					}
					totalLineCount++;
				}
			}

			ylineCount--;
		}

	}

	private void CreateMobPaths( TileData[] tilesData )
	{
		int mobPath = 3;

		for ( int i = 0 ; i < mobPath ; ++i )
		{
			//check if section is not obstruded
			int index = Random.Range(0, sections.Count);
			int section = sections[index];
			List<int> directions = GetSideDirection(section);

			//print("mob path gen " + directions.Count);
			if ( directions.Count > 0 )
			{
				for ( int j = 0 ; j < directions.Count ; ++j )
				{
					//check if neighbor is not obstruded
					if ( tilesData[directions[j]].obstacle == TileObstacle.none )
					{
						mobPaths.Add( new List<int>() );
						int nextPosition = directions[j];
						int step = nextPosition - section;
						//print("section "+ section+" nextPosition " + nextPosition + " step " + step);

						tilesData[section].obstacle = TileObstacle.mobPath;
						tilesData[nextPosition].obstacle = TileObstacle.mobPath;
						mobPaths[i].Add( section );
						mobPaths[i].Add( nextPosition );

						nextPosition += step;
						while ( nextPosition > playerCornerIndex &&
							!sections.Contains( nextPosition ) &&
							isDirectionValid( nextPosition ) &&
							tilesData[nextPosition] != null &&
							tilesData[nextPosition].obstacle == TileObstacle.none )
						{
							tilesData[nextPosition].obstacle = TileObstacle.mobPath;

							//print("new nextPosition " + nextPosition );
							mobPaths[i].Add( nextPosition );
							nextPosition += step;
						}
						break;

					}
					else
					{
						continue;
					}
				}
			}

			//section checked
			sections.RemoveAt( index );
		}
	}

	private void CreateWalls( TileData[] tilesData )
	{
		int wallCount = 3;

		for(int i = 0 ; i < wallCount ; ++i )
		{
			bool isWallSet = false;

			while(!isWallSet)
			{
				int index = Random.Range(1, width*height);

				TileData tileData = tilesData[index];

				if ( tileData != null &&
					tileData.type == TileType.wall &&
					tileData.obstacle == TileObstacle.none)
				{
					isWallSet = true;
					tilesData[index].obstacle = TileObstacle.brickWall;
				}
			}
		}
	}

	List<int> GetSideDirection( int position )
	{
		List<int> directions = new List<int>();
		int north = position+width;
		int east = position+1;
		int south = position-width;
		int west = position -1;

		if ( north < width * height)
		{
			//print("north " + north);
			directions.Add( north );
		}
		if ( east < width * height && (east / width == position / width) )
		{
			//print("east " + east);
			directions.Add( east );
		}
		if ( south > 0 )
		{
			//print("south " + south);
			directions.Add( south );
		}
		if ( west > 0 && (west / width == position / width) )
		{
			//print("west " + west);
			directions.Add( west );
		}

		return directions;
	}

	bool isDirectionValid( int position )
	{
		if ( position > 0 && position < (width * height)-1 )
		{
			return true;
		}

		return false;
	}

	void GenerateDebugTiles( TileData[] tilesData )
	{
		GameObject.DestroyImmediate( parent );
		parent = GameObject.Instantiate( mapParent );

		for ( int i = 0 ; i < tilesData.Length ; ++i )
		{
			TileData tileData = tilesData[i];
			if ( tileData != null )
			{
				GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cube.transform.SetParent( parent.transform );
				cube.GetComponent<BoxCollider>().enabled = false;
				float y = 0f;
				if(tileData.x == 0f && tileData.y == 0f)
                {
					y = 0.25f;
                }
				cube.transform.position = new Vector3( tileData.x, y, tileData.y );
				cube.transform.Rotate( Vector3.up, tileData.rotation );

				MeshRenderer renderer = cube.GetComponent<MeshRenderer>();
				switch ( tileData.type )
				{
					case TileType.corner:
					renderer.sharedMaterial = testMat[0];
					break;
					case TileType.cross:
					renderer.sharedMaterial = testMat[1];
					break;
					case TileType.deadEnd:
					renderer.sharedMaterial = testMat[2];
					break;
					case TileType.tSection:
					renderer.sharedMaterial = testMat[3];
					break;
					case TileType.wall:
					renderer.sharedMaterial = testMat[4];
					break;
					default:
					break;
				}

				/*if ( tileData.obstacle == TileObstacle.brickWall )
				{
					GameObject brickWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
					brickWall.transform.SetParent( parent.transform );
					brickWall.GetComponent<BoxCollider>().enabled = false;
					brickWall.transform.position = new Vector3( tileData.x, 1f, tileData.y );
					brickWall.transform.localScale = Vector3.one * .3f;
					brickWall.name = "brickWall";
				}*/

			}
		}

		foreach(List<int> mobPath in mobPaths )
		{
				print("Path: ");
			foreach(int position in mobPath)
			{
				int x = position % width;
				int y = position / width;
				print("x: "+x+" y: "+y);

				GameObject mobPathSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				mobPathSphere.transform.SetParent( parent.transform );
				mobPathSphere.GetComponent<SphereCollider>().enabled = false;
				mobPathSphere.transform.position = new Vector3( x, 1f, y );
				mobPathSphere.transform.localScale = Vector3.one * .3f;
				mobPathSphere.name = "mobPathSphere "+position;
			}
		}
	}

	void GenerateMapTiles( TileData[] tilesData )
	{
		GameObject.DestroyImmediate( parent );
		parent = GameObject.Instantiate( mapParent );

		for ( int i = 0 ; i < tilesData.Length ; ++i )
		{
			TileData tileData = tilesData[i];
			if ( tileData != null )
			{
				GameObject prefab = GameObject.Instantiate(GetMapPrefab(tileData.type));
				prefab.transform.SetParent( parent.transform );
				prefab.transform.position = new Vector3( tileData.x * tileSize, 0f, -tileData.y * tileSize );
				prefab.transform.Rotate( Vector3.up, tileData.rotation );

				if ( tileData.obstacle == TileObstacle.brickWall )
				{
					GameObject wallPrefab = GameObject.Instantiate(wallPrefabs[0]);
					wallPrefab.transform.SetParent( parent.transform );
					wallPrefab.transform.position = new Vector3( tileData.x * tileSize, 0.5f, -tileData.y * tileSize );
					wallPrefab.transform.Rotate( Vector3.up, tileData.rotation + 90 );
				}
			}
		}

		CreatMobs();
	}

	void CreatMobs()
	{
		foreach ( List<int> mobPath in mobPaths )
		{
			List<Vector3> mobPathVec3 = new List<Vector3>();
			foreach ( int position in mobPath )
			{
				int x = position % width;
				int y = position / width;

				mobPathVec3.Add( new Vector3( x * tileSize, 0f, -y * tileSize ) );
			}
			mobSpawner.CreateMob( mobPathVec3, parent.transform );
		}
	}

	GameObject GetMapPrefab( TileType type )
	{
		switch ( type )
		{
			case TileType.corner:
			return mapPrefabs[0];
			case TileType.cross:
			return mapPrefabs[1];
			case TileType.deadEnd:
			return mapPrefabs[2];
			case TileType.tSection:
			return mapPrefabs[3];
			case TileType.wall:
			return mapPrefabs[4];
			default:
			break;
		}
		return null;
	}
}
