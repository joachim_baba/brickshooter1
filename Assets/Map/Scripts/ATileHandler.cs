﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ATileHandler : MonoBehaviour, ITileHandler
{
    public virtual void CreateTile() { }

    public int GetRandom(int total)
    {
        return Random.Range(0, total);
    }

    protected void CreateProp(Transform parent, GameObject[] assets)
    {
        GameObject propInstance = GameObject.Instantiate(assets[GetRandom(assets.Length)]);

        propInstance.transform.SetParent(parent);
        propInstance.transform.localPosition = Vector3.zero;
        propInstance.transform.localRotation = Quaternion.identity;
        propInstance.transform.localScale = Vector3.one;
    }

    protected void CreatePropSerie(Transform[] parents, GameObject[] assets)
    {
        foreach(Transform parent in parents)
        {
            CreateProp(parent, assets);
        }
    }

}
