﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerTileHandler : ATileHandler
{
    [SerializeField]
    Transform intCorner, extCorner;

    [SerializeField]
    GameObject[] intCornerAsset, extCornerAsset;

    // Start is called before the first frame update
    void Start()
    {
        CreateTile();
    }

    private void Update()
    {
        Debug.DrawLine(intCorner.position, extCorner.position, Color.red);
    }

    override public void CreateTile()
    {
        CreateProp(intCorner, intCornerAsset);
        CreateProp(extCorner, extCornerAsset);
    }
}
