﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTileHandler : ATileHandler
{
    [SerializeField]
    Transform[] walls;

    [SerializeField]
    GameObject[] wallAsset;

    // Start is called before the first frame update
    void Start()
    {
        CreateTile();
    }

    private void Update()
    {
        for (int i = 0; i < walls.Length; ++i)
        {
            int j = i + 1 >= walls.Length ? 0 : i + 1;
            Debug.DrawLine(walls[i].position, walls[j].position, Color.red);

        }
    }
    override public void CreateTile()
    {
        CreatePropSerie(walls, wallAsset);
    }
}
