﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerationTrigger : MonoBehaviour
{
    [SerializeField]
    MapGenerator mapGenerator;

	private void Start()
	{
		mapGenerator.GenerateMap();
	}
}
