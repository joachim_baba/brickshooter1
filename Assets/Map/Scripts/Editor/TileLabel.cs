﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// Create a 180 degrees wire arc with a ScaleValueHandle attached to the disc
// lets you visualize some info of the transform

[CustomEditor(typeof(TileLabel))]
class LabelHandle : Editor
{
    void OnSceneGUI()
    {
        TileLabel handleExample = (TileLabel)target;
        if (handleExample == null)
        {
            return;
        }

        Handles.color = Color.blue;
        Handles.Label(handleExample.transform.position + Vector3.up * 4,
            handleExample.name);

        Handles.BeginGUI();
        if (GUILayout.Button("Reset Area", GUILayout.Width(100)))
        {
        }
        Handles.EndGUI();


        //Handles.DrawWireArc(handleExample.transform.position,
        //    handleExample.transform.up,
        //    -handleExample.transform.right,
        //    180,
        //    handleExample.shieldArea);
        //handleExample.shieldArea =
        //    Handles.ScaleValueHandle(handleExample.shieldArea,
        //        handleExample.transform.position + handleExample.transform.forward * handleExample.shieldArea,
        //        handleExample.transform.rotation,
        //        1,
        //        Handles.ConeHandleCap,
        //        1);
    }
}