﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MapGenerator mapGenerator = (target as MapGenerator);
        
        mapGenerator.mapParent = EditorGUILayout.ObjectField("mapParent", (mapGenerator.mapParent), typeof(GameObject), true) as GameObject;
        mapGenerator.mobSpawner = EditorGUILayout.ObjectField( "mobSpawner", (mapGenerator.mobSpawner), typeof( MobSpawner), true) as MobSpawner;

        EditorGUILayout.LabelField("width");
        mapGenerator.width = EditorGUILayout.IntSlider(mapGenerator.width, 6, 11);
        EditorGUILayout.LabelField("height");
        mapGenerator.height = EditorGUILayout.IntSlider(mapGenerator.height, 6, 11);

        var serializedObject = new SerializedObject(target);
        var property = serializedObject.FindProperty("testMat");
        var property2 = serializedObject.FindProperty("mapPrefabs");
        var property3 = serializedObject.FindProperty("wallPrefabs");
        serializedObject.Update();
        EditorGUILayout.PropertyField(property, true);
        EditorGUILayout.PropertyField(property2, true);
        EditorGUILayout.PropertyField(property3, true);
        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("generate Map"))
        {
            mapGenerator.GenerateMap();
        }

        if (GUILayout.Button("generate Map Debug"))
        {
            mapGenerator.GenerateMap(isDebug : true);
        }
    }

}
