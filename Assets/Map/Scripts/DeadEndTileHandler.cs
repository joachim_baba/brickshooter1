﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadEndTileHandler : ATileHandler
{
    [SerializeField]
    Transform[] cornerShorts;

    [SerializeField]
    GameObject[] cornerShortAsset;

    // Start is called before the first frame update
    void Start()
    {
        CreateTile();
    }
    private void Update()
    {
        for (int i = 0; i < cornerShorts.Length; ++i)
        {
            int j = i + 1 >= cornerShorts.Length ? 0 : i + 1;
            Debug.DrawLine(cornerShorts[i].position, cornerShorts[j].position, Color.red);

        }
    }

    override public void CreateTile()
    {
        CreatePropSerie(cornerShorts, cornerShortAsset);
    }
}
