﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossTileHandler : ATileHandler
{
    [SerializeField]
    Transform[] corners;

    [SerializeField]
    GameObject[] cornerAsset;

    // Start is called before the first frame update
    void Start()
    {
        CreateTile();
    }

    private void Update()
    {
        for (int i = 0; i < corners.Length; ++i)
        {
            int j = i + 1 >= corners.Length ? 0 : i + 1;
            Debug.DrawLine(corners[i].position, corners[j].position, Color.red);

        }
    }

    override public void CreateTile()
    {
        CreatePropSerie(corners, cornerAsset);
    }
}
