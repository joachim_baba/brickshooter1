﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSectionTileHandler : ATileHandler
{
    [SerializeField]
    Transform[] corners;

    [SerializeField]
    Transform wall;

    [SerializeField]
    GameObject[] cornerAsset;

    [SerializeField]
    GameObject[] wallAssets;

    // Start is called before the first frame update
    void Start()
    {
        CreateTile();
    }

    private void Update()
    {
        for (int i = 0; i < corners.Length; ++i)
        {
            int j = i + 1 >= corners.Length ? 0 : i + 1;
            Debug.DrawLine(corners[i].position, corners[j].position, Color.red);

        }
    }

    override public void CreateTile()
    {
        CreateProp(wall, wallAssets);
        CreatePropSerie(corners, cornerAsset);
    }
}
