﻿
using UnityEngine;

public interface ITileHandler
{
    void CreateTile();
}
