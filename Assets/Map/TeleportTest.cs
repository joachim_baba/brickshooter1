﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportTest : MonoBehaviour
{
    public Transform teleportDestination;
    bool isTeleported = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<CharacterBodyControls>().enabled = false;
            isTeleported = true;
            other.gameObject.transform.position = teleportDestination.position;
            other.gameObject.transform.rotation = Quaternion.LookRotation(teleportDestination.forward);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isTeleported)
            {
                isTeleported = false;
                other.GetComponent<CharacterBodyControls>().enabled = true;
            }
        }
    }

}
