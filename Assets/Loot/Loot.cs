﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : MonoBehaviour
{
    Timer idlePositionTimer, idleRotationTimer;

    float floatingScale = 0.25f;

    public static int lootCount = 0;

    private void Awake()
    {
        GetComponent<MeshRenderer>().material = LootMaterials.GetSomeMaterial();
    }

    // Start is called before the first frame update
    void Start()
    {
        float originalY = transform.position.y;
        float poisitionAnimationDuration = 2f;
        idlePositionTimer = Timer.NormalizedTimer(poisitionAnimationDuration, loops: true);
        idlePositionTimer.addOnUpdateDelegate = delegate (float t)
        {
            Vector3 position = transform.position;
            transform.position = new Vector3(position.x, originalY + Easing.Easings.SineEaseIn( Mathf.Sin(t * Mathf.PI)) * floatingScale, position.z);
        };
        idlePositionTimer.Update(Random.value * poisitionAnimationDuration);
        idlePositionTimer.play();

        idleRotationTimer = Timer.NormalizedTimer(6f, loops: true);
        idleRotationTimer.addOnUpdateDelegate = delegate (float t)
        {
            transform.rotation = Quaternion.AngleAxis(t * 360, Vector3.up);
        };
        idleRotationTimer.play();
        
        lootCount++;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        idlePositionTimer.stop();
        idleRotationTimer.stop();
    }
}
