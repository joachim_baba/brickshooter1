﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootGenerator : MonoBehaviour
{
    [SerializeField]
    GameObject[] lootTemplates;
    [SerializeField]
    Transform[] lootPositions;

	void Awake()
	{
		foreach ( Transform lootPosition in lootPositions )
		{
			GameObject template = lootTemplates[Random.Range(0, lootTemplates.Length)];
			GameObject.Instantiate( template, lootPosition.position, Quaternion.AngleAxis( Random.value * 360f, Vector3.up ), transform.parent);
		}
	}
}
