﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootMaterials : MonoBehaviour
{
    [SerializeField]
    Material[] _materials;

    static LootMaterials _instance;

    static Material[] materials
    {
        get
        {
            return _instance._materials;
        }
    }

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
        }
    }

    public static Material GetSomeMaterial()
    {
        return materials[Random.Range(0, materials.Length)];
    }
}
