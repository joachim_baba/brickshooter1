﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawner : MonoBehaviour
{
	[SerializeField]
	public GameObject shellTemplate;

	[SerializeField]
	public GameObject mobTemplate;

	static MobSpawner _instance;

	private void Awake()
	{
		if( _instance == null)
		{
			_instance = this;	
		}
	}

	public void CreateMob(List<Vector3> mobPath, Transform parent)
	{
		GameObject mob = GameObject.Instantiate( mobTemplate, mobPath[0], Quaternion.identity, parent);
		mob.GetComponent<Mob>().positionsList = mobPath.ToArray();
	}

	public static void CreateShell(Vector3 shellPosition, Quaternion shellRotation)
    {
        GameObject.Instantiate( _instance.shellTemplate, shellPosition, shellRotation );
    }
}
