﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobDroneState : AMobState
{
    int currentPointIndex = 0;
    Vector3 currentPoint;
    float displacementSpeed = 5f;

    public MobDroneState(Mob linkedMob)
    {
        mob = linkedMob;
        UpdatePoint();
    }
    
    public override void Update(Vector3 playerPosition)
    {
        MoveToPoint();

        Vector3 dir = DirToPlayer(playerPosition);
        float dotProducToPlayer = DotProducToPlayer(dir);

        if (dotProducToPlayer > sightFrustrum && dir.magnitude < sightDistance)
        {
            bool isPlayerVisible = IsPlayerVisible(dir);
            if (isPlayerVisible)
            {
                //MonoBehaviour.print("MobDroneState player !");
                mob.ChangeState();
            }
        }
    }

    void MoveToPoint()
    {
        Vector3 direction = currentPoint - mob.transform.position;

        if (direction.magnitude > 0.05f)
        {
            mob.transform.position += direction.normalized * Time.deltaTime * displacementSpeed;
            mob.transform.LookAt(currentPoint, Vector3.up);
        }
        else
        {
            currentPointIndex++;
            currentPointIndex %= mob.positionsList.Length;
            UpdatePoint();
        }
    }

    void UpdatePoint()
    {
        if( mob.positionsList != null && mob.positionsList.Length >0 )
		{
            currentPoint = mob.positionsList[currentPointIndex];
		}
    }

}
