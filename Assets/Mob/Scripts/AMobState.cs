﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AMobState : IMobState
{
	Mob _mob;

	public const float sightFrustrum = 0.5f;
	public const float sightDistance = 8f;

	public Mob mob
	{
		get
		{
			return _mob;
		}
		set
		{
			_mob = value;
		}
	}

	public virtual void Update(Vector3 playerPosition)
	{
	}

	public Vector3 DirToPlayer(Vector3 playerPosition)
    {
		return playerPosition - mob.transform.position;
	}

	public float DotProducToPlayer(Vector3 dirToPlayer)
    {
		return Vector3.Dot(mob.transform.forward, dirToPlayer.normalized);
    }

	public bool IsPlayerVisible(Vector3 dirToPlayer)
    {
		Ray ray = new Ray(mob.transform.position, dirToPlayer.normalized);
		RaycastHit raycastHit;
		bool hit = Physics.Raycast(ray, out raycastHit, dirToPlayer.magnitude + 1f);
		Collider raycastCollider = raycastHit.collider;

		return hit && raycastCollider != null && raycastCollider.CompareTag("Player");
	}
}
