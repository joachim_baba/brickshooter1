﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobHeart : MobBrick
{
    [SerializeField]
    Mob mob;

    override protected void OnCollisionEnter( Collision collision )
    {
        mob.HeartDestroyedNotification();
        base.OnCollisionEnter( collision );
        ApplyBallHit();
    }
}
