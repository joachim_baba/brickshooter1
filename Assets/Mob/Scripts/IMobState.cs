﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMobState
{
	Mob mob { get; set; }

	void Update(Vector3 playerPosition);
}
