﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class Mob : MonoBehaviour
{
    [SerializeField]
    public Transform RaycastOrigin;

    [SerializeField]
    MobBrickDestructionHandler brickDestructionHandler;

    [SerializeField]
    ParticleSystem _particleSystem;

    [SerializeField]
    AudioClip[] audioClips;

    AudioSource audioSource;

    public Transform[] positionsTransformList;
    public Vector3[] positionsList;

    IMobState _currentState;

    bool isHeartHit = false;

    Gun _gun;

    Timer shakeTimer;
    int shakeDirection = 1;

    public Gun gun
    {
        get
        {
            return _gun;
        }
    }

    private void Awake()
    {
        //ExtractPositionsList();
        _gun = GetComponent<Gun>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _currentState = new MobDroneState(this);
        CreateShakeTimer();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isHeartHit)
        {
            CharacterBodyControls playerInstance = CharacterBodyControls.instance;
            Vector3 playerPosition = -transform.forward;

            if(playerInstance != null)
            {
                playerPosition = playerInstance.transform.position;
            }
            _currentState.Update(playerPosition);
        }
    }

    public void ChangeState()
    {
        if (_currentState != null)
        {
            IMobState newState;
            if (_currentState.GetType() == typeof(MobDroneState))
            {
                newState = new MobAttackState(this);

            }
            else
            {
                newState = new MobDroneState(this);
            }

            _currentState = newState;
        }
    }

    public async void HeartDestroyedNotification()
    {
        isHeartHit = true;
        _particleSystem.Play();
        audioSource.PlayOneShot(audioClips[0]);
        Timer bowTimer = CreateBowAnimation();
        await bowTimer.playAsync();

        audioSource.PlayOneShot(audioClips[1]);
        shakeTimer.play();
        await brickDestructionHandler.DestroyBody();
        shakeTimer.stop();
        audioSource.Stop();
        if( _particleSystem!=null )
		{
            _particleSystem.Stop();
		}

        await Task.Delay(500);
        MobSpawner.CreateShell(transform.position, transform.rotation);
        GameObject.Destroy(this.gameObject);
    }

    public void ExtractPositionsList()
    {
        if (positionsTransformList != null && positionsTransformList.Length > 0)
        {
            positionsList = new Vector3[positionsTransformList.Length];
            for (int i = 0; i < positionsTransformList.Length; ++i)
            {
                positionsList[i] = positionsTransformList[i].position;
            }
        }
    }

    Timer CreateBowAnimation()
    {
        Timer bowTimer = Timer.NormalizedTimer(1f, Easing.Easings.Functions.QuadraticEaseOut, delay: 0.15f);
        Quaternion initialRotation = transform.rotation;
        Quaternion bowRotation = transform.rotation * Quaternion.AngleAxis(10, Vector3.right);

        bowTimer.addOnUpdateDelegate = delegate (float t)
        {
            transform.rotation = Quaternion.Lerp(initialRotation, bowRotation, t);
        };
        return bowTimer;
    }

    void CreateShakeTimer()
    {
        shakeTimer = new Timer(10);
        shakeTimer.addOnUpdateDelegate = delegate (float t)
        {
            transform.position += new Vector3(GetRandomShakeValue(), GetRandomShakeValue());
            shakeDirection = -shakeDirection;
        };
    }

    float GetRandomShakeValue()
    {
        return (Random.value - 0.5f) * 0.02f * shakeDirection;
    }
}
