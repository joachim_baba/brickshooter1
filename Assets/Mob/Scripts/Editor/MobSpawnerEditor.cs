﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( MobSpawner ) )]
public class MobSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
	{
		MobSpawner mobSpawner = (target as MobSpawner);

		mobSpawner.mobTemplate = EditorGUILayout.ObjectField( "mobTemplate", (mobSpawner.mobTemplate), typeof( GameObject ), true ) as GameObject;
		mobSpawner.shellTemplate = EditorGUILayout.ObjectField( "shellTemplate", (mobSpawner.shellTemplate), typeof( GameObject ), true ) as GameObject;
	}
}
