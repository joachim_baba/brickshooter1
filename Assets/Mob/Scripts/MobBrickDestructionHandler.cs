﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class MobBrickDestructionHandler : MonoBehaviour
{
    MobBrick[] _mobBricks;
    int[] deathOrderList;

    // Start is called before the first frame update
    void Start()
    {
        _mobBricks = GetComponentsInChildren<MobBrick>();
        deathOrderList = new int[_mobBricks.Length];
        for (int i = 0; i < deathOrderList.Length; ++i)
        {
            deathOrderList[i] = i;
        }

        for (int i = 0; i < deathOrderList.Length; ++i)
        {
            int switchedIndex = Random.Range(0, deathOrderList.Length);
            int currentValue = deathOrderList[i];
            int switchedValue = deathOrderList[switchedIndex];
            deathOrderList[i] = switchedValue;
            deathOrderList[switchedIndex] = currentValue;
        }
    }

    public async Task DestroyBody()
    {
        for (int i = 0; i < _mobBricks.Length; ++i)
        {
            if (_mobBricks[i] != null)
            {
                _mobBricks[i].SelectForDestruction();
            }
        }

        int length = _mobBricks.Length;
        for (int i = 0; i < length; ++i)
        {
            float index = i;
            float lengthF = length;
            float progress = index / lengthF;
            float accelertion = Easing.Easings.CubicEaseIn(progress);
            int delayReduction = Mathf.RoundToInt(accelertion * 30);

            if (_mobBricks[deathOrderList[i]] != null)
            {
                await Task.Delay(50 - delayReduction);
                _mobBricks[deathOrderList[i]].ApplyBallHit();
            }
        }
    }
}
