﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobAttackState : AMobState
{
    bool targetInitiated = false;
    float lookAtProgression = 0f;

    Timer changeStateDelay, shootTimer;

    public MobAttackState(Mob linkedMob)
    {
        mob = linkedMob;
    }

    public override void Update(Vector3 playerPosition)
    {
        bool exitAttack = true;

        Vector3 dir = DirToPlayer(playerPosition);
        float dotProducToPlayer = DotProducToPlayer(dir);

        if (dotProducToPlayer > sightFrustrum)
        {

            bool isPlayerVisible = IsPlayerVisible(dir);
            if (isPlayerVisible)
            {
                exitAttack = false;
                //TargetLook( dir );
                Quaternion destination = Quaternion.LookRotation(dir.normalized, Vector3.up);
                mob.transform.rotation = destination;

                TargetCheck();
            }
        }

        if (exitAttack && !targetInitiated)
        {
            CancelTargetCheck();
            mob.transform.rotation = Quaternion.LookRotation(mob.transform.forward, Vector3.up);
        }
    }

    void TargetLook(Vector3 direction)
    {
        Quaternion destination = Quaternion.LookRotation(direction.normalized, Vector3.up);

        if (lookAtProgression < 1f)
        {
            Quaternion mobRotation = mob.transform.rotation;
            lookAtProgression += Time.deltaTime * 0.1f;

            mob.transform.rotation = Quaternion.Lerp(mobRotation, destination, lookAtProgression);
        }
        else
        {
            mob.transform.rotation = destination;
        }
    }

    void TargetCheck()
    {
        if (!targetInitiated && shootTimer == null)
        {
            //MonoBehaviour.print("TargetCheck " + targetInitiated);
            targetInitiated = true;

            shootTimer = new Timer(1f);
            shootTimer.addOnCompleteDelegate = delegate ()
            {
                shootTimer = null;
                if (targetInitiated)
                {
                    ResetTarget();
                    Shoot();
                }
            };
            shootTimer.play();
        }
    }

    void CancelTargetCheck()
    {
        //MonoBehaviour.print("CancelTargetCheck " + targetInitiated);
        if (targetInitiated)
        {
            ResetTarget();

        }

        if (changeStateDelay == null)
        {
            changeStateDelay = new Timer(2f);
            changeStateDelay.addOnCompleteDelegate = delegate ()
            {
                if (!targetInitiated)
                {
                    mob.ChangeState();
                }
                changeStateDelay = null;
            };
            changeStateDelay.play();
        }
    }

    void Shoot()
    {
        if (mob != null)
        {
            //MonoBehaviour.print("Shoot");
            Transform origin = mob.RaycastOrigin;
            mob.gun.Shoot(origin.position, origin.forward);
        }
    }

    void ResetTarget()
    {
        targetInitiated = false;
        lookAtProgression = 0f;
    }
}
