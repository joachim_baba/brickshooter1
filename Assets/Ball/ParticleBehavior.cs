﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleBehavior : MonoBehaviour
{
    Timer timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = new Timer(2.5f);
        timer.addOnCompleteDelegate = delegate ()
        {
            GameObject.Destroy(this.gameObject);
        };
        timer.play();
    }
}
