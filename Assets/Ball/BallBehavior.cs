﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehavior : MonoBehaviour
{
    [SerializeField]
    AudioClip[] audioClips;

    [SerializeField]
    GameObject ballParticlesTemplate;

    float liveDuration = 10.0f;

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        liveDuration -= Time.deltaTime;
        if(liveDuration <= 0f)
        {
            InstanciateParticles();
            GameObject.Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        AudioClip audioClip = audioClips[Random.Range(0, audioClips.Length)];
        audioSource.PlayOneShot(audioClip);
    }

    void InstanciateParticles()
    {
        GameObject.Instantiate(ballParticlesTemplate, transform.position, transform.rotation);
    }
}
