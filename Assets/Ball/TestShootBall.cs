﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShootBall : MonoBehaviour
{
    public Transform ball;
    public Transform ball2;

    // Start is called before the first frame update
    void Start()
    {
        ball.GetComponent<Rigidbody>().AddForce(Vector3.up + Vector3.left + Vector3.forward, ForceMode.Impulse);
        ball2.GetComponent<Rigidbody>().AddForce(Vector3.left, ForceMode.Impulse);
    }
}
